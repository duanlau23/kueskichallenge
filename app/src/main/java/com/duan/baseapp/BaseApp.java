package com.duan.baseapp;

import android.app.Activity;
import android.app.Application;

import com.duan.baseapp.di.components.DaggerAppComponent;
import com.google.gson.Gson;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

/**
 * File Description
 * <p>
 * Author: Lajesh D
 * Email: lajeshds2007@gmail.com
 * Created: 7/24/2018
 * Modified: 7/24/2018
 */
public class BaseApp extends Application implements HasActivityInjector {

    private static BaseApp sInstance;


    public static BaseApp getAppContext() {
        return sInstance;
    }



    private static synchronized void setInstance(BaseApp app) {
        sInstance = app;
    }
    @Inject
    DispatchingAndroidInjector<Activity> activityDispatchingInjector;

    @Inject
    Gson gson;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeComponent();
        setInstance(this);
    }

    private void initializeComponent() {
        DaggerAppComponent.builder()
                .application(this)
                .build()
                .inject(this);
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityDispatchingInjector;
    }

    public Gson getGson() {
        return gson;
    }
}
