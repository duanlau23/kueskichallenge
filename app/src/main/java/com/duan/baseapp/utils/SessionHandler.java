package com.duan.baseapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.duan.baseapp.common.KeyNames;

import java.util.Date;

public class SessionHandler {

    private static SessionHandler instance;
    private static SharedPreferences pref;

    public static synchronized SessionHandler init(Context context){
        if(instance != null){
            return  instance;
        }

        return new SessionHandler(context);
    }

    public static synchronized SessionHandler getInstance(Context context){
        if(instance != null){
            return instance;
        }

        return init(context);
    }

    public static synchronized SessionHandler getInstance() throws NullPointerException {
        if(instance != null){
            return instance;
        }

        throw new NullPointerException();
    }

    private SessionHandler(Context context) {
        pref = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
    }

    public static void setUser(String user){

        pref.edit().putString(KeyNames.USER, user).apply();
    }

    public static String getUser(){
        return pref.getString(KeyNames.USER, "");
    }

    public static void setPassword(String password){

        pref.edit().putString(KeyNames.PASSWORD, password).apply();
    }

    public static String getPassword(){
        return pref.getString(KeyNames.PASSWORD, "");
    }

    public static void setToken(String token){

        pref.edit().putString(KeyNames.TOKEN, token).apply();
    }

    public static String getToken(){
        return pref.getString(KeyNames.TOKEN, "");
    }

    public static void setUserId(String token){

        pref.edit().putString(KeyNames.USER_ID, token).apply();
    }

    public static String getUserId(){
        return pref.getString(KeyNames.USER_ID, "");
    }


    public static void setExpiresAt(Long expiresAt){
        pref.edit().putLong(KeyNames.EXPIRES_AT, expiresAt).apply();
    }

    public static Long getExpiresAt(){
        return pref.getLong(KeyNames.EXPIRES_AT, 0);
    }

    public static void setUserType(Integer userType){
        pref.edit().putInt(KeyNames.USER_TYPE, userType).apply();
    }

    public static Integer getUserType(){
        return pref.getInt(KeyNames.USER_TYPE, 0);
    }

    public static boolean isExpired(){
        Long expire = getExpiresAt();
        Long curr = new Date(System.currentTimeMillis()).getTime()/1000;
        return getExpiresAt() < System.currentTimeMillis() / 1000;
    }

    public static void resetToken(){
        setExpiresAt(0L);
        setToken("");

    }
}