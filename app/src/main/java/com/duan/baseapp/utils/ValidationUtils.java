package com.duan.baseapp.utils;

import android.text.TextUtils;
import android.widget.EditText;

public class ValidationUtils {

    public static void validateLength(
            EditText editText,
            String text,
            Integer minLength,
            Integer maxLength){

        if (TextUtils.isEmpty(text)) {
            editText.setError(null);
            return;
        }
        if (editText.getText().toString().length() < minLength) {
            editText.setError("Password must be minimum " + minLength + " length");
            return;
        }

        if (editText.getText().toString().length() > maxLength) {
            editText.setError("Password must be maximum " + maxLength + " length");
            return;
        }

        editText.setError(null);
    }

    public static void validateLength(EditText editText, String text,  Integer minLength){
        validateLength(editText, text, minLength, 0);
    }

    public static void validateEmail(EditText editText, String email){
        // Minimum a@b.c
        if (email != null) {
            int indexOfAt = email.indexOf("@");
            int indexOfDot = email.lastIndexOf(".");
            if (indexOfAt > 0 && indexOfDot > indexOfAt && indexOfDot < email.length() - 1) {
                editText.setError(null);
            } else {
                editText.setError("Wrong email format");
            }
        }
    }

    public static void validatePhone(EditText editText, String phone){
        //TODO validate phone
    }

}
