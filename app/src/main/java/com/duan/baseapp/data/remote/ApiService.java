package com.duan.baseapp.data.remote;

import com.duan.baseapp.data.remote.model.TwilioResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiService {

    @POST("2010-04-01/Accounts/{appId}/Messages.json")
    @FormUrlEncoded
    Call<TwilioResponse> sendCode(
            @Path("appId") String appId,
            @Field("Body") String body,
            @Field("From") String from,
            @Field("To") String to,
            @Header("Authorization")String credentials,
            @Header("Accept") String accept);


}
