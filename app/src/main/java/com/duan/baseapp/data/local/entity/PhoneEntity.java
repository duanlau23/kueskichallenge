package com.duan.baseapp.data.local.entity;

import androidx.annotation.NonNull;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.duan.baseapp.BR;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "phones")
public class PhoneEntity extends BaseObservable {

    @PrimaryKey
    @NonNull
    @SerializedName("phone")
    @ColumnInfo(name = "phone")
    private String phone;
    @SerializedName("code")
    @ColumnInfo(name = "code")
    private String code;
    @SerializedName("verified")
    private boolean verified;

    @Bindable
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
        notifyPropertyChanged(com.duan.baseapp.BR.phone);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }
}
