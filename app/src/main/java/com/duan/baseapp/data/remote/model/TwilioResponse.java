package com.duan.baseapp.data.remote.model;

import com.google.gson.annotations.SerializedName;

public class TwilioResponse {

    @SerializedName("body")
    private String body;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
