package com.duan.baseapp.data.remote;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

public abstract class LocalBoundResource<T> {

    private final MediatorLiveData<Resource<T>> result = new MediatorLiveData<>();

    @MainThread
    protected LocalBoundResource() {
        result.setValue(Resource.loading(null));

        // Load data from db
        LiveData<T> dbSource = loadFromDb();

        result.addSource(dbSource, newData -> {
            if(null != newData){
                result.setValue(Resource.success(newData));
            }else{
                result.setValue(Resource.error("Not found", null));
            }
        });
    }

    @NonNull
    @MainThread
    protected abstract LiveData<T> loadFromDb();


    public final LiveData<Resource<T>> getAsLiveData() {
        return result;
    }
}
