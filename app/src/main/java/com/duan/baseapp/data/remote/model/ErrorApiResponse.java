package com.duan.baseapp.data.remote.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ErrorApiResponse {
    @SerializedName("status")
    private String status = null;
    @SerializedName("message")
    private String message = null;
    @SerializedName("statusCode")
    private Integer statusCode;
    @SerializedName("method")
    private String method = null;
    @SerializedName("fields")
    private List<String> fields = null;
    @SerializedName("error")
    private String error = null;
    @SerializedName("error_description")
    private String error_description = null;

    public String getStatus() {
        return this.status;
    }

    public String getMessage() {
        return this.message;
    }

    public Integer getStatusCode() {
        return this.statusCode;
    }

    public String getMethod() {
        return this.method;
    }

    public String getError() {
        return this.error;
    }

    public String getErrorDescription() {
        return this.error_description;
    }

    public List<String> getFields() {
        return this.fields;
    }

}
