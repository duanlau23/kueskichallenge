package com.duan.baseapp.data.local.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.duan.baseapp.data.local.entity.PhoneEntity;

import java.util.List;

@Dao
public interface PhoneDao {

    @Query("SELECT * FROM phones")
    LiveData<List<PhoneEntity>> getPhones();

    @Query("SELECT * FROM phones WHERE phone = :phone LIMIT 1")
    LiveData<PhoneEntity> findPhoneByNumber(String phone);

    @Query("SELECT * FROM phones WHERE code = :code LIMIT 1")
    LiveData<PhoneEntity> findPhoneByCode(String code);

    @Query("SELECT COUNT(*) FROM phones WHERE code = :code")
    LiveData<Integer> getPhoneCountByCode(String code);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void savePhone(PhoneEntity phoneEntity);

}
