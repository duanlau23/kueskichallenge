package com.duan.baseapp.data.remote.repository;

import android.annotation.SuppressLint;
import android.os.AsyncTask;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.duan.baseapp.common.Constants;
import com.duan.baseapp.data.local.dao.PhoneDao;
import com.duan.baseapp.data.local.entity.PhoneEntity;
import com.duan.baseapp.data.remote.ApiService;
import com.duan.baseapp.data.remote.LocalBoundResource;
import com.duan.baseapp.data.remote.NetworkBoundResource;
import com.duan.baseapp.data.remote.Resource;
import com.duan.baseapp.data.remote.model.TwilioResponse;
import com.duan.baseapp.utils.AbsentLiveData;
import com.duan.baseapp.utils.CipherUtils;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;

public class PhoneRepository {

    private final PhoneDao phoneDao;
    private final ApiService apiService;
    private final MediatorLiveData<Resource<PhoneEntity>> result = new MediatorLiveData<>();

    @Inject
    PhoneRepository(PhoneDao dao, ApiService service) {
        this.phoneDao = dao;
        this.apiService = service;
    }

    public LiveData<Resource<PhoneEntity>> findPhoneByCode(String code) {
        return new LocalBoundResource<PhoneEntity>() {
            @NonNull
            @Override
            protected LiveData<PhoneEntity> loadFromDb() {
                return phoneDao.findPhoneByCode(code);
            }
        }.getAsLiveData();
    }


    public LiveData<Resource<List<PhoneEntity>>> getPhones() {
        return new LocalBoundResource<List<PhoneEntity>>() {
            @NonNull
            @Override
            protected LiveData<List<PhoneEntity>> loadFromDb() {
                return phoneDao.getPhones();
            }
        }.getAsLiveData();
    }


    public void savePhone(PhoneEntity phoneEntity) {
        saveResultAndReInit(phoneEntity);
    }

    @SuppressLint("StaticFieldLeak")
    @MainThread
    public void saveResultAndReInit(PhoneEntity phoneEntity) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                phoneDao.savePhone(phoneEntity);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {

            }
        }.execute();
    }

    public LiveData<Resource<PhoneEntity>> findPhoneByNumber(String number) {
        return new LocalBoundResource<PhoneEntity>() {
            @NonNull
            @Override
            protected LiveData<PhoneEntity> loadFromDb() {
                return phoneDao.findPhoneByNumber(number);
            }
        }.getAsLiveData();
    }

    public LiveData<Resource<PhoneEntity>> verifyCode(PhoneEntity phoneEntity){
        return new LocalBoundResource<PhoneEntity>() {
            @NonNull
            @Override
            protected LiveData<PhoneEntity> loadFromDb() {
                return phoneDao.findPhoneByCode(phoneEntity.getCode());
            }
        }.getAsLiveData();
    }

    public LiveData<Resource<TwilioResponse>> sendCode(PhoneEntity phoneEntity){
        return new NetworkBoundResource<TwilioResponse, TwilioResponse>() {
            private TwilioResponse resultsDb;
            @Override
            protected void saveCallResult(TwilioResponse loginResponse) {
                resultsDb = loginResponse;
                savePhone(phoneEntity);
            }

            @NonNull
            @Override
            protected LiveData<TwilioResponse> loadFromDb() {
                if (resultsDb == null) {
                    return AbsentLiveData.create();
                }else {
                    return new LiveData<TwilioResponse>() {
                        @Override
                        protected void onActive() {
                            super.onActive();
                            setValue(resultsDb);
                        }
                    };
                }
            }

            @NonNull
            @Override
            protected Call<TwilioResponse> createCall() {
                phoneEntity.setCode(CipherUtils.generateCode());
                return apiService.sendCode(
                        Constants.APP_ID,
                        CipherUtils.generateCode(),
                        Constants.FROM_NUMBER,
                        "+52" + phoneEntity.getPhone(),
                        "Basic QUNjZWIyNTM2NjZmZDYyMDE2MTJkYWQ3NjAxNmVjNGI3Nzo0ZTEzYjQxZTQwYzc1NGQ0MjFkY2ZhYWNiZGU4MjY3Nw==",
                        "application/json");
            }
        }.getAsLiveData();
    }

}
