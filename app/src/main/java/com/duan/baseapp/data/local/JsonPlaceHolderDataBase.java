package com.duan.baseapp.data.local;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.duan.baseapp.data.local.dao.PhoneDao;
import com.duan.baseapp.data.local.entity.PhoneEntity;

/**
 * File Description
 * <p>
 * Author: Lajesh D
 * Email: lajeshds2007@gmail.com
 * Created: 7/24/2018
 * Modified: 7/24/2018
 */
@Database(
        entities =
                {
                        PhoneEntity.class
                },
        version = 4,
        exportSchema = false)
public abstract class JsonPlaceHolderDataBase extends RoomDatabase {

    public abstract PhoneDao phoneDao();

}