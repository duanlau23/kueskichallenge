package com.duan.baseapp.viewmodel;

import android.widget.EditText;

import androidx.databinding.BindingAdapter;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.duan.baseapp.data.local.entity.PhoneEntity;
import com.duan.baseapp.data.remote.Resource;
import com.duan.baseapp.data.remote.model.TwilioResponse;
import com.duan.baseapp.data.remote.repository.PhoneRepository;
import com.duan.baseapp.utils.ValidationUtils;

import java.util.List;

import javax.inject.Inject;

public class PhoneViewModel extends ViewModel {

    private final LiveData<Resource<List<PhoneEntity>>> phones;
    private final PhoneRepository phoneRepository;
    private PhoneEntity phoneEntity;

    @Inject
    public PhoneViewModel(PhoneRepository phoneRepository) {
        this.phoneRepository = phoneRepository;
        this.phones = phoneRepository.getPhones();
        phoneEntity = new PhoneEntity();
    }

    public LiveData<Resource<List<PhoneEntity>>> getPhones() {
        return phones;
    }

    public PhoneEntity getPhoneEntity() {
        return phoneEntity;
    }

    public LiveData<Resource<TwilioResponse>> sendCode(){
        return phoneRepository.sendCode(phoneEntity);
    }

    public LiveData<Resource<PhoneEntity>> verifyCode(){
        return phoneRepository.verifyCode(phoneEntity);
    }

    public void savePhoneEntity(PhoneEntity phoneEntity){
        phoneRepository.savePhone(phoneEntity);
    }

    @BindingAdapter("phoneValidator")
    public static void passwordValidator(EditText editText, String password) {
        ValidationUtils.validatePhone(editText, password);
    }



}
