package com.duan.baseapp.di.builder;

import com.duan.baseapp.view.activity.CodeVerificationActivity;
import com.duan.baseapp.view.activity.PhoneListActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * The module which provides the android injection service to Activities.
 * Author: Lajesh D
 * Email: lajeshds2007@gmail.com
 * Created: 7/24/2018
 * Modified: 7/24/2018
 */
@Module
public abstract class ActivityBuilderModule {

    @SuppressWarnings("unused")
    @ContributesAndroidInjector(modules = FragmentBuilderModule.class)
    abstract CodeVerificationActivity codeVerificationActivity();

    @ContributesAndroidInjector(modules = FragmentBuilderModule.class)
    abstract PhoneListActivity phoneListActivity();

}
