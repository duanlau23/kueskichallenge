package com.duan.baseapp.di.module;

import android.app.Application;

import androidx.room.Room;

import com.duan.baseapp.BaseApp;
import com.duan.baseapp.data.local.JsonPlaceHolderDataBase;
import com.duan.baseapp.data.local.dao.PhoneDao;
import com.duan.baseapp.data.remote.ApiConstants;
import com.duan.baseapp.data.remote.ApiService;
import com.duan.baseapp.data.remote.RequestInterceptor;
import com.duan.baseapp.utils.SessionHandler;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * The application module which provides app wide instances of various components
 * Author: Lajesh D
 * Email: lajeshds2007@gmail.com
 * Created: 7/24/2018
 * Modified: 7/24/2018
 */
@Module(includes = ViewModelModule.class)
public class AppModule {

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {
        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
        okHttpClient.connectTimeout(ApiConstants.CONNECT_TIMEOUT, TimeUnit.MILLISECONDS);
        okHttpClient.readTimeout(ApiConstants.READ_TIMEOUT, TimeUnit.MILLISECONDS);
        okHttpClient.writeTimeout(ApiConstants.WRITE_TIMEOUT, TimeUnit.MILLISECONDS);
        okHttpClient.addInterceptor(new RequestInterceptor());
        okHttpClient.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        return okHttpClient.build();
    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    ApiService provideRetrofit(OkHttpClient okHttpClient) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        return retrofit.create(ApiService.class);
    }

    @Provides
    @Singleton
    JsonPlaceHolderDataBase provideArticleDatabase(Application application) {
        return Room.databaseBuilder(application, JsonPlaceHolderDataBase.class, "articles.db").build();
    }

    @Provides
    @Singleton
    PhoneDao providePhoneDao(JsonPlaceHolderDataBase jsonPlaceHolderDataBase) {
        return jsonPlaceHolderDataBase.phoneDao();
    }

    @Provides
    @Singleton
    SessionHandler provideSessionHandler(JsonPlaceHolderDataBase jsonPlaceHolderDataBase){
        return SessionHandler.init(BaseApp.getAppContext());
    }

}
