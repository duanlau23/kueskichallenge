package com.duan.baseapp.di.module;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.duan.baseapp.viewmodel.PhoneViewModel;
import com.duan.baseapp.viewmodel.ViewModelFactory;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * Alloes us to inject dependencies via constructor injection
 * <p>
 * Author: Lajesh D
 * Email: lajeshds2007@gmail.com
 * Created: 7/24/2018
 * Modified: 7/24/2018
 */
@Module
public abstract class ViewModelModule {


    @Binds
    @IntoMap
    @ViewModelKey(PhoneViewModel.class)
    @SuppressWarnings("unused")
    abstract ViewModel bindsPhoneViewModel(PhoneViewModel phoneViewModel);


    @Binds
    @SuppressWarnings("unused")
    abstract ViewModelProvider.Factory bindsViewModelFactory(ViewModelFactory viewModelFactory);
}
