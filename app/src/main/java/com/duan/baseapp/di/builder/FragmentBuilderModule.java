package com.duan.baseapp.di.builder;

import com.duan.baseapp.view.fragment.CodeVerificationFragment;
import com.duan.baseapp.view.fragment.PhoneListFragment;
import com.duan.baseapp.view.fragment.SendCodeFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * This builder provides android injector service to fragments
 * Author: Lajesh D
 * Email: lajeshds2007@gmail.com
 * Created: 7/24/2018
 * Modified: 7/24/2018
 */
@Module
public abstract class FragmentBuilderModule {


    @SuppressWarnings("unused")
    @ContributesAndroidInjector
    abstract SendCodeFragment contributeSendCodeFragment();

    @SuppressWarnings("unused")
    @ContributesAndroidInjector
    abstract CodeVerificationFragment contributeCodeVerificationFragment();


    @SuppressWarnings("unused")
    @ContributesAndroidInjector
    abstract PhoneListFragment contributePhoneListFragment();

}
