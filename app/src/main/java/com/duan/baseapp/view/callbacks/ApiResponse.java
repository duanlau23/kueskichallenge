package com.duan.baseapp.view.callbacks;

public interface ApiResponse <T>{

    void onSuccess(T data);

    void onFailed(String message);

}
