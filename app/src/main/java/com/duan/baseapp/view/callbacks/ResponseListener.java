package com.duan.baseapp.view.callbacks;

public interface ResponseListener<D> {

    void onSuccess(D data);
    void onFailure(String message);

}
