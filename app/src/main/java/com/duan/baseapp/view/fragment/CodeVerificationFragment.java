package com.duan.baseapp.view.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.duan.baseapp.R;
import com.duan.baseapp.data.remote.Status;
import com.duan.baseapp.databinding.FragmentCodeVerificationBinding;
import com.duan.baseapp.view.base.BaseFragment;
import com.duan.baseapp.viewmodel.PhoneViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class CodeVerificationFragment extends BaseFragment<PhoneViewModel, FragmentCodeVerificationBinding>
        implements View.OnClickListener {



    public CodeVerificationFragment() {
        // Required empty public constructor
    }

    public static CodeVerificationFragment newInstance() {
        Bundle args = new Bundle();
        CodeVerificationFragment fragment = new CodeVerificationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dataBinding.setPhoneViewModel(viewModel);
        dataBinding.setPhoneEntity(viewModel.getPhoneEntity());
        dataBinding.setOnClickListener(this);
    }

    @Override
    protected Class<PhoneViewModel> getViewModel() {
        return PhoneViewModel.class;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_code_verification;
    }

    @Override
    public void onClick(View v) {
        viewModel
                .verifyCode()
                .observe(this, resource -> {
                    dataBinding.setResource(resource);
                    if(
                            resource.data != null &&
                            resource.status == Status.SUCCESS){

                        resource.data.setVerified(true);
                        viewModel.savePhoneEntity(resource.data);
                        Toast.makeText(
                                getContext(),
                                getString(R.string.valid_code),
                                Toast.LENGTH_SHORT).show();
                    }else if(resource.status != Status.LOADING){
                        Toast.makeText(
                                getContext(),
                                getString(R.string.invalid_code),
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
