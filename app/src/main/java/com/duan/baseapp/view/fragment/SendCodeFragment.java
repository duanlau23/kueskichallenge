package com.duan.baseapp.view.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.duan.baseapp.R;
import com.duan.baseapp.data.remote.Status;
import com.duan.baseapp.databinding.FragmentSendCodeBinding;
import com.duan.baseapp.utils.FragmentUtils;
import com.duan.baseapp.view.activity.PhoneListActivity;
import com.duan.baseapp.view.base.BaseFragment;
import com.duan.baseapp.viewmodel.PhoneViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class SendCodeFragment
        extends BaseFragment<PhoneViewModel, FragmentSendCodeBinding>
        implements View.OnClickListener {


    public SendCodeFragment() {
        // Required empty public constructor
    }

    public static SendCodeFragment newInstance() {
        Bundle args = new Bundle();
        SendCodeFragment fragment = new SendCodeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dataBinding.setPhoneViewModel(viewModel);
        dataBinding.setPhoneEntity(viewModel.getPhoneEntity());
        dataBinding.setOnClickListener(this);
    }

    @Override
    protected Class<PhoneViewModel> getViewModel() {
        return PhoneViewModel.class;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_send_code;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_send_code:
                sendCode();
                break;
            case R.id.btn_show_list:
                openPhoneListActivity();
                break;
            default:
        }
    }

    private void sendCode(){
        viewModel
                .sendCode()
                .observe(this, twilioResponse -> {
                    dataBinding.setResource(twilioResponse);

                    if(twilioResponse.status == Status.SUCCESS){
                        showCodeVerificationFragment();
                    }
                });
    }

    private void openPhoneListActivity(){
        Intent intent = new Intent(getActivity(), PhoneListActivity.class);
        startActivity(intent);
    }

    private void showCodeVerificationFragment(){
        FragmentUtils.replaceFragment(
                getActivity(),
                new CodeVerificationFragment(),
                R.id.fragment_container,
                false,
                FragmentUtils.TRANSITION_NONE
        );
    }
}
