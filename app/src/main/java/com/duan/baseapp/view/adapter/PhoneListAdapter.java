package com.duan.baseapp.view.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.duan.baseapp.data.local.entity.PhoneEntity;
import com.duan.baseapp.view.adapter.view_holders.PhoneViewHolder;
import com.duan.baseapp.view.base.BaseAdapter;
import com.duan.baseapp.view.callbacks.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class PhoneListAdapter extends BaseAdapter<PhoneViewHolder, PhoneEntity> {

    private OnItemClickListener<PhoneEntity> onItemClickListener;
    private List<PhoneEntity> phoneEntities;

    public PhoneListAdapter(OnItemClickListener<PhoneEntity> onItemClickListener){
        this.onItemClickListener = onItemClickListener;
        phoneEntities = new ArrayList<>();
    }

    @Override
    public void setData(List<PhoneEntity> data) {
        this.phoneEntities = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PhoneViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return PhoneViewHolder.create(LayoutInflater.from(parent.getContext()), parent, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull PhoneViewHolder holder, int position) {
        holder.onBind(phoneEntities.get(position));
    }

    @Override
    public int getItemCount() {
        return phoneEntities.size();
    }
}
