package com.duan.baseapp.view.activity;

import android.os.Bundle;

import com.duan.baseapp.R;
import com.duan.baseapp.databinding.ActivityPhoneListBinding;
import com.duan.baseapp.utils.FragmentUtils;
import com.duan.baseapp.view.base.BaseActivity;
import com.duan.baseapp.view.fragment.PhoneListFragment;
import com.duan.baseapp.view.fragment.SendCodeFragment;

public class PhoneListActivity extends BaseActivity<ActivityPhoneListBinding> {

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_phone_list;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FragmentUtils.replaceFragment(
                this,
                PhoneListFragment.newInstance(),
                R.id.fragment_container,
                false,
                FragmentUtils.TRANSITION_NONE
        );
    }
}
