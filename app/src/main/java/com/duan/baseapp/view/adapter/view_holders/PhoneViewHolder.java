package com.duan.baseapp.view.adapter.view_holders;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.duan.baseapp.data.local.entity.PhoneEntity;
import com.duan.baseapp.databinding.ItemPhoneListBinding;
import com.duan.baseapp.view.callbacks.OnItemClickListener;

public class PhoneViewHolder extends RecyclerView.ViewHolder {

    public static PhoneViewHolder create(LayoutInflater inflater, ViewGroup parent, OnItemClickListener<PhoneEntity> callback) {
        ItemPhoneListBinding itemMovieListBinding = ItemPhoneListBinding.inflate(inflater, parent, false);
        return new PhoneViewHolder(itemMovieListBinding, callback);
    }

    private final ItemPhoneListBinding binding;

    public PhoneViewHolder(ItemPhoneListBinding binding, OnItemClickListener<PhoneEntity> callback) {
        super(binding.getRoot());
        this.binding = binding;
        binding.getRoot().setOnClickListener(v ->
                callback.onItemClicked(binding.getPhoneEntity()));
    }

    public void onBind(PhoneEntity postEntity) {
        binding.setPhoneEntity(postEntity);
        binding.executePendingBindings();
    }

}
