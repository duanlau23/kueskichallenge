package com.duan.baseapp.view.activity;

import android.os.Bundle;

import com.duan.baseapp.R;
import com.duan.baseapp.databinding.ActivityCodeVerificationBinding;
import com.duan.baseapp.utils.FragmentUtils;
import com.duan.baseapp.view.base.BaseActivity;
import com.duan.baseapp.view.fragment.SendCodeFragment;

public class CodeVerificationActivity extends BaseActivity<ActivityCodeVerificationBinding> {

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_code_verification;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FragmentUtils.replaceFragment(
                this,
                SendCodeFragment.newInstance(),
                R.id.fragment_container,
                false,
                FragmentUtils.TRANSITION_NONE
        );
    }
}
