package com.duan.baseapp.view.callbacks;

public interface OnItemClickListener<D> {

    void onItemClicked(D item);

}
