package com.duan.baseapp.view.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.duan.baseapp.R;
import com.duan.baseapp.data.local.entity.PhoneEntity;
import com.duan.baseapp.data.remote.Status;
import com.duan.baseapp.databinding.FragmentPhoneListBinding;
import com.duan.baseapp.view.adapter.PhoneListAdapter;
import com.duan.baseapp.view.base.BaseFragment;
import com.duan.baseapp.view.callbacks.OnItemClickListener;
import com.duan.baseapp.viewmodel.PhoneViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhoneListFragment extends
        BaseFragment<PhoneViewModel, FragmentPhoneListBinding> implements
        OnItemClickListener<PhoneEntity> {


    public PhoneListFragment() {
        // Required empty public constructor
    }

    public static PhoneListFragment newInstance() {
        Bundle args = new Bundle();
        PhoneListFragment fragment = new PhoneListFragment();
        fragment.setArguments(args);
        return fragment;
    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        dataBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        dataBinding.recyclerView.setAdapter(new PhoneListAdapter(this));
        return dataBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        viewModel.getPhones()
                .observe(this, listResource -> {
                    if(null != listResource && (listResource.status == Status.ERROR || listResource.status == Status.SUCCESS)){
                        dataBinding.loginProgress.setVisibility(View.GONE);
                    }
                    dataBinding.setResource(listResource);

                    // If the cached data is already showing then no need to show the error
                    if(null != dataBinding.recyclerView.getAdapter() && dataBinding.recyclerView.getAdapter().getItemCount() > 0){
                        dataBinding.errorText.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public Class<PhoneViewModel> getViewModel() {
        return PhoneViewModel.class;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_phone_list;
    }

    @Override
    public void onItemClicked(PhoneEntity item) {

    }
}