package com.duan.baseapp.common;

public class KeyNames {

    private KeyNames(){}

    public static final String
        POST_ID = "postId",
        ALBUM_ID = "albumId",
        USER = "user",
        PASSWORD = "password",
        TOKEN = "token",
        USER_ID = "USER_ID",
        EXPIRES_AT = "expires_at",
        USER_TYPE = "user_type";

}
