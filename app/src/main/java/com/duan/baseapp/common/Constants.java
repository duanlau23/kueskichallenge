package com.duan.baseapp.common;

import okhttp3.Credentials;

public class Constants {

    public static final String TWILIO_AUTH_TOKEN = "4e13b41e40c754d421dcfaacbde82677";
    public static final String APP_ID = "ACceb253666fd6201612dad76016ec4b77";
    public static final String FROM_NUMBER = "+14153407702";
    public static final String CREDENTIALS = Credentials.
            basic(Constants.APP_ID, Constants.TWILIO_AUTH_TOKEN);


    private Constants(){
        // Private constructor to hide the implicit one
    }

}


//curl -X POST https://api.twilio.com/2010-04-01/Accounts/ACceb253666fd6201612dad76016ec4b77/Messages.json \
//        --data-urlencode "Body=This is the ship that made the Kessel Run in fourteen parsecs?" \
//        --data-urlencode "From=+14153407702" \
//        --data-urlencode "To=+526681135713" \
//        -u ACceb253666fd6201612dad76016ec4b77:4e13b41e40c754d421dcfaacbde82677